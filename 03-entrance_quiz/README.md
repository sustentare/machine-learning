Leitura base: "O que é aprendizado máquina: supervisionado e não supervisionado?". [Link]( https://www.wrprates.com/aprendizado-de-maquina-supervisionado-e-nao-supervisionado/).

## Perguntas

Responda com V para verdadeiro ou F para falso.

1. ( ) Os métodos de classificação não servem para predizer variáveis binárias. <!-- FALSO, servem para as binárias -->
2. ( ) Em modelos supervisionados conseguimos avaliar um modelo pelo seu nível de assertividade ou precisão. <!-- Verdadeiro -->
3. ( ) Em análises com modelos não supervisionados não se divide o conjunto de dados em "treino" e "teste". <!-- Verdadeiro -->
4. ( ) Definir a pergunta de negócio que irá gerar valor à organização é mais importante do que o nível de assertividade de um modelo de *machine learning*. <!-- V -->
5. ( ) Não há modelo de *machine learning* que possa ser enquadrado em "classifcação" e "regressão", apenas em um ou outro. <!-- FALSO -->

Escolha uma alternativa por pergunta.

**6. Quando há um *target* a ser estudado em um modelo, falamos que é do tipo:**

  a. Regressão.
  
  b. Classificação.
  
  c. Supervisionado. <!-- Gabarito -->
  
  d. Não supervisionado.
  
  
**7. Modelos que buscam encontrar como uma variável se comporta na medida em que outras sofrem alterações, sendo muito utilizados em análises de séries temporais (como em finanças e meteorologia, por exemplo), são do tipo:**

  a. Clustering.
  
  b. Regressão.
  
  c. Árvore de decisão.
  
  d. K-means.
  

**8. Em uma análise exploratória de definição de perfil de clientes, qual das abordagens de *machine learning* seria mais recomendada:**

  a. Modelos supervisionados de classificação.
  
  b. Modelos econométricos de regressão. 
  
  c. Clustering não supervisionado. <!-- Gabarito, pois não há target definido --> 
  
  d. Não se aplica *machine learning* para este caso.
  

**9. Em uma análise de definição de perfil de clientes que geram *churn* (cancelamento de assinatura, medida de forma binária: 0 = não cancelou o serviço; 1 = cancelou o serviço) na assinatura de um *software*, qual das abordagens de *machine learning* seria mais recomendada:** 

  a. Modelos supervisionados de classificação. <!-- Como há um target, modelos supervisionados de classificação são os mais recomendados. -->
  
  b. Modelos econométricos de regressão. 
  
  c. Clustering não supervisionado. 
  
  d. Não se aplica *machine learning* para este caso.


## Gabarito

1. FALSO, servem para as binárias, que são tratadas como categóricas.
2. VERDADEIRO.
3. VERDADEIRO. São análises exploratórias, sem uma variável *target* definida.
4. VERDADEIRO. De nada adianta um modelo extramamente assertivo, mas não agrega algo novo para a organização. Ter um olhar sistêmico e com visão no negócio é essencial para obter sucessos em projetos de *machine learning*.
5. FALSO. Modelos de árvore de decisão, por exemplo, podem ser utilizados tanto para regressão quanto para classificação.
6. C.
7. B.
8. C. Não há uma variável *target* definida, então uma abordagem não supervisionada, como clsutering, é mais recomendado.
9. A. Como há um *target*, modelos supervisionados de classificação são os mais recomendados.
