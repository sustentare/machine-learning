# Top 10 vídeos de *Machine Learning*

## Objetivo
Proporcionar maior entendimento e visão dos temas / tópicos relacionados à *machine learning*.

## Top 10 vídeos

1. **Introdução à Machine Learning by StatQuest** [En]

<https://www.youtube.com/watch?v=Gv9_4yMHFhI>

2. **Introdução à Machine Learning by TV Mackenzie** [Pt]

<https://www.youtube.com/watch?v=ZlqmgYj0VCU>

3. ***Crash Course* de aprendizado de máquina by Google** [En]

<https://developers.google.com/machine-learning/crash-course/ml-intro> 


4. **Playlist com 494 vídeos de Machine Learning by Udacity** [En]

<https://www.youtube.com/playlist?list=PLAwxTw4SYaPkQXg8TkVdIvYv4HfLG7SiH>

5. **Receitas para o aprendizado de máquina by Google Developers** [En]

<https://www.youtube.com/watch?v=cKxRvEZd3Mw>

6. **Medindo a performance de modelos com curva de ganhos e lift** [En]

<https://www.youtube.com/watch?v=IwCUZQllVVI>

7. **Explicando a curva ROC e área sob a curva** [Pt]

<https://www.youtube.com/watch?v=wmXZ6d6j6Ps&t=1527s>

8. **Matriz confusão by StatQuest** [En]

<https://www.youtube.com/watch?time_continue=3&v=Kdsp6soqA7o>

9. **Redes Neurais e *Deep Learning*, do conceito à aplicação** [Pt]

<https://www.youtube.com/watch?v=KIvB5LFbA0w>

10. **Como se tornar um cientista de dados?** [En]

<https://www.youtube.com/watch?v=-AkBfBWr_Gw>