# Programa de Aula

## Calendário

- Datas: 15, 16, 29 e 30 de março de 2019.
- Disciplina: "Modelos e *Frameworks* de *Machine Learning*" 
- Curso: "Inteligência Artificial, Big Data e Negócios"
- Professor: Wlademir Ribeiro Prates

## Descritivo da Disciplina

### Ementa
Computadores que aprendem. Decisão e predição baseados em dados. Fundamentos de avaliação e validação de modelos. Aprendizagem supervisionada, aprendizagem não-supervisionada, aprendizagem por reforço e deep learning. Ferramentas de *machine learning*. Algoritmos confiáveis, rápidos e escaláveis. Construção, implementação e manutenção de sistemas de *machine learning*. Métricas dos impactos dos sistemas de *machine learning*. *Machine learning* como estratégia corporativa.


### Objetivos
1. Entender a importância de trabalhar com análises preditivas e prescritivas para decisões *data driven* nas organizações.
2. Compreender conceitualmente os modelos de *machine learning* apresentados, como avaliá-los e quando aplicá-los.
3. Ter pensamento crítico para ser capaz de gerar valor às decisões de negócios com *machine learning*.

### Conteúdo programático

#### Aula 1 - 15/03/2019 (sexta)
- Por que *machine learning* é importante nos negócios?
- Análise preditiva e prescritiva.
- Fluxo de *machine learning*.
- Aplicação prática do fluxo de *machine learning* (pacote Radiant do R).
- Regressão e classificação.

#### Aula 2 - 16/03/2019 (sábado)
- Aprendizagem supervisionada:
  - Regressão linear, regressão logística, árvores de decisão, random forest, redes neurais.
- Aprendizagem não-supervisionada:
  - Clustering, PCA.
- Aprendizagem por reforço e deep learning.
- Avaliação de modelos.
- Instruções para trabalho individual.

#### Aula 3 - 29/03/2019 (sexta)
- H2O - plataforma  Open Source de *machine learning*.
- Sparklyr (Interface R para Apache Spark).

#### Aula 4 - 30/03/2019 (sábado)
- Revisão geral.
- Trabalho em grupo.

### Metodologia
- Apresentação de slides;
- Uso de quadro branco para explicações pontuais e exemplos;
- Exercícios em computador (replicações de exemplos dos slides);
- Atividades em grupos.

#### Requisitos
- Linguagem R - [Site R Project](https://www.r-project.org/) - [Download](https://cloud.r-project.org/)
- Inslação Rstudio (IDE para o R) - [Download versão Open Source](https://www.rstudio.com/products/rstudio/download/)
- [Recomendável] Fazer cadastro no Gitlab e ter conhecimentos básicos de GIT - [link](https://gitlab.com/)
- Instalacação de pacote Radiant do R - [link](https://radiant-rstats.github.io/docs/index.html)
- Instalação do pacote H2O para R - [link](http://docs.h2o.ai/h2o/latest-stable/h2o-docs/welcome.html)
 
### Avaliação

| Atividade                  | Peso |
|----------------------------|------|
| Q1 - *Entrance* Quiz       | 2,0  |
| AI - Avaliação individual  | 3,0  |
| AG - Avaliação em grupo    | 5,0  |

AI - Cada aluno deverá criar um fluxo de análise com *machine learning* e entregar na forma de relatório, utilizando conjuntos de dados disponibilizados em aula.

AG - Os grupos irão entregar e apresentar um relatório com um modelo de predição. Preferencialmente deve ser utilizado um conjunto de dados que o próprio grupo irá conseguir. Deverão ser utilizados preferencialmente H2O ou Spark MLIB.
<!-- - *Entrance* Quiz individual (Plataforma SER): Peso 2,0 -->
<!-- - Atividades na plataforma SER: Peso 1,0 -->
<!-- - Auto-avaliação: Peso 1,0 -->
<!-- - Trabalho finala: Peso 6,0 -->


### Bibliografia básica
PROVOST, F., & FAWCETT, T. (2016). *Data Science paea Negócios*. Rio Janeiro: Alta Books.

BURGUER, S. V. (2018). *Introduction to Machine Learning with R*. O'Reilly.

### Bibliografia complementar
ZHENG, A. (2015). *Evaluating Machine Learning Models*. O'Reilly.

COOK, D. (2017). *Practical Machine Learning with H2O*. O'Reilly.

### Links sugeridos

#### Materiais
- [Biblioteca Virtual](http://sustentare.bv3.digitalpages.com.br/users/sign_in)
- [Pasta compartilhada / Download dos materiais](https://goo.gl/PP1ZXU)
- [Repositório de códigos para a disciplina no GitLab](https://gitlab.com/sustentare/machine-learning/)

#### Leitura recomendada
- [Aprendizado de máquina: supervisionado e não supervisionado | wrprates.com](https://www.wrprates.com/aprendizado-de-maquina-supervisionado-e-nao-supervisionado/)

- [Análise descritiva, preditiva, prescritiva e cenarização: como gerar valor nos negócios](https://www.aquare.la/analise-descritiva-preditiva-prescritiva-e-cenarizacao/)

- [Controlling *machine learning* algorithms and their biases](https://drive.google.com/file/d/1yJWCJfUTDgMpfnuPfMiFhIaQpT9LxXyO/view?usp=sharing)

#### Leitura opcional

- [Demystifying AI and *machine learning* for executives](https://drive.google.com/file/d/1FXWIlqYJKO53bXN0orpOnVPMTDWdjcY4/view?usp=sharing)

- [O que é árvore de decisão (decision tree)? Exemplos em R | wrprates.com](https://www.wrprates.com/o-que-e-arvore-de-decisao-decision-tree-linguagem-r/)
 

#### Documentações de frameworks de *machine learning*
- [Documentação do *framework* de ML "H2O" (**muito completo**)](http://docs.h2o.ai/h2o/latest-stable/h2o-docs/index.html)

- [Spark *Machine Learning* Library (MLlib)](https://spark.rstudio.com/mlib/)

- [Detalhes do pacote de análise de dados "Radiant" do R](https://radiant-rstats.github.io/docs/index.html)

#### Dados abertos
- [University of Ccalifornia Irvine - *Machine Learning* Repository](https://archive.ics.uci.edu/ml/index.php)
- [Portal Brasileiro de Dados Abertos](http://dados.gov.br/)
- [Kaggle Datasets](https://www.kaggle.com/datasets)
<!-- - [Gov Data](https://govdata.gov.br/) -->

#### *Free Courses*
- [Google *Crash Course* de Introdução à *machine learning*](https://developers.google.com/machine-learning/crash-course/)

## Currículo resumido do professor
Atualmente é Cientista de Dados Senior na Ernst & Young (EY), trabalhando com projetos em grandes empresas voltados à RH (*HR Analytics*) na área de *People Advisory Services*. Entre os temas de projetos estão: análise de *turnover*, clima e sucessão. 

Foi Cientista de Dados na Aquarela Advanced Analytics, tendo participado em projetos de *Data Analytics* nas mais diversas áreas, como manutenção de aeronaves, logística (redução de falhas em entregas), gestão de estoques, redução de *churn*.

Doutor e Mestre em Finanças pelo Programa de Pós-Graduação em Administração da Universidade Federal de Santa Catarina. Bacharel em Administração de Empresas pela Faculdade Anhanguera de Passo Fundo. 

Na tese de doutorado trabalhou com modelos econométricos aplicados a uma base de 60 milhões de operações de compra e venda de ações da B3 (a bolsa de valores brasileira), analisando o comportamento das decisões com base na literatura de Finanças Comportamentais.

Professor universitário e de programas de MBA tanto em disciplinas voltadas para a área de Finanças, quanto para *Data Science*.

Entre as principais *skills* estão: programação em R, métodos econométricos, modelos de *machine learning*, visualização de dados (*reports* e *dashboards* interativos).

Contatos:

- [CV Lattes](http://lattes.cnpq.br/5885808985726209)
- [LinkedIn](https://www.linkedin.com/in/wlademir-ribeiro-prates/)
