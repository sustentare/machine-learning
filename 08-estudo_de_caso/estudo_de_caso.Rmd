---
title: "Estudo de caso de *Machine Learning* | Análise preditiva para logística"
author: "Wlademir Ribeiro Prates"
output:
  word_document: default
  #pdf_document: default
  
urlcolor: blue
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

```


Uma grande empresa da área de logística é responsável por realizar entregas diárias de uma importante marca de calçados em todo o país. As entregas são feitas diretamente nos estabelecimentos comerciais (em geral lojistas que são clientes da marca de calçados). As datas e horários das entregas são sempre definidas pela marca (cliente da empresa de logística) e seus lojistas, sendo repassadas para a empresa de logística sempre na sexta feira da semana anterior às entregas. 

O planejamento funciona bem, e a empresa de logística tem grande rigor no cumprimento dos horários das entregas. Na prática, porém, os lojistas não recebem cerca de 5% das entregas feitas pelos caminhões da empresa de logística, mesmo que o caminhão chegue na hora acordada. Isso ocorre por `n` motivos, praticamente sempre por culpa do lojista que não pôde receber (exemplos: falta de espaço para armazenamento, horário de pico no movimento da loja, etc).

Cada entrega não realizada representa um grande custo para a empresa de logística, uma vez que por questões contratuais, é ela a responsável pelos custos de conduzir a mercadoria até o centro logístico e seguir novamente para a entrega em um novo horário. 

A empresa de logística quer melhorar a assertividade das entregas. Ela não define os horários, mas têm autonomia para sugerir horários diferentes de entregas ao seu cliente. A marca de calçados também tem interesse em uma solução que reduza esse 5% de falhas nas entregas, pois a "não entrega" no horário combinado gera transtornos aos lojistas, incluindo a falta de mercadorias por alguns dias.

Neste contexto, responda:

- Você acredita que *machine learning* pode resolver esta situação? Por quê?

- Qual seria uma possível pergunta de negócio para guiar as análises?

- Você utilizaria aprendizagem supervisionada ou não supervisionada?

- Há uma variável *target*? Se sim, qual poderia ser?

- Que tipos de dados você poderia sugerir para a empresa de logística coletar a fim de treinar em um modelo de *machine learning*?
