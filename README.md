# README

Este é um reposítório de códigos para a disciplina de "Modelos e Frameworks de Machine Learning", ministrada pelo prof. Wlademir Ribeiro Prates na faculdade Sustentare.

Você encontrará aqui:

- Códigos em Rmarkdown para compilar os slides;
- Exercícios;
- Links para leitura recomendada;
- Algoritmos de machine learning transmistidos em aula, etc.