# Top 10 textos de *Machine Learning*

## Objetivo
Proporcionar maior entendimento e visão dos temas / tópicos relacionados à *machine learning*.

## Top 10 textos

1. **Análise descritiva, preditiva, prescritiva e cenarização: como gerar valor nos negócios** - Esclarece as diferenças entre tipos de análise de dados, entedimento essencial antes de entrar no estudo de *machine learning*. [Pt]

<https://www.aquare.la/analise-descritiva-preditiva-prescritiva-e-cenarizacao/> 
  
  2. **Aprendizado de máquina: supervionado e não supervisionado** - Explica as principais divisões de temas em *machine learning*, com exemplos práticos na linguagem `R`. [Pt]

<https://www.wrprates.com/aprendizado-de-maquina-supervisionado-e-nao-supervisionado/>
  
  3. **Tutoriais de `R` com aplicações para diversos modelos de machine learning** - Conjunto de vários tutoriais breves e objetivos, que demonstram as possibilidades de aplicações de modelos de *machine learning* utilizando a linguagem `R`. [En]

<https://data-flair.training/blogs/clustering-in-r-tutorial/>
  
  
  4. **Escolhendo a melhor métrica para modelos de regressão** - Apresenta um resumo das principais métricas de avaliação de modelos de regressão. [En]

<https://medium.com/usf-msds/choosing-the-right-metric-for-machine-learning-models-part-1-a99d7d7414e4>
  
  5. **Escolhendo a melhor métrica para modelos de classificação** - Apresenta um resumo das principais métricas de avaliação de modelos de classificação. [En]

<https://medium.com/usf-msds/choosing-the-right-metric-for-evaluating-machine-learning-models-part-2-86d5649a5428>
  
  6. **Cases brasileiros de aplicações de machine learning** - Postagens com cases de aplicações de ML do site Medium. [Pt]

<https://medium.com/ensina-ai/cases-brasileiros/home>
  
  7. **O que é árvore de decisão (decision tree)? Exemplos em R** - Explicação sobre árvores de decisão e os conceitos envolvidos, como pureza, entropia e ganho de informação. [Pt]

<https://www.wrprates.com/o-que-e-arvore-de-decisao-decision-tree-linguagem-r/>
  
  8. **Redes Neurais, Perceptron Multicamadas e o Algoritmo Backpropagation** - Detalhamento de redes neurais. Material denso, com alto aprofundamento matemático, mas com linguagem de fácil compreensão. [Pt]

<https://medium.com/ensina-ai/redes-neurais-perceptron-multicamadas-e-o-algoritmo-backpropagation-eaf89778f5b8>
  
  9. **Categoria de textos de Machine Learning do site Towards Data Science | Medium** - Excelentes postagens sobre a área de aprendizado de máquina como um todo. [En]

<https://towardsdatascience.com/machine-learning/home>
  
  10. **Tutoriais de machine learning** - Explicações objetivas sobre diversos modelos de aprendizagem de máquina, com exemplos na linguagem *Python*. [En]

<https://data-flair.training/blogs/machine-learning-tutorial/>
  
  